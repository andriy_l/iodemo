package com.brainacad.io;

import java.io.*;

/**
 * Created by andriy on 20.02.17.
 */
private class ReplacementInFile {
    private static void main(String[] args) {
        String srcFile = "files" + File.separator + "F.java";
        StringBuilder sb = new StringBuilder();
        try(BufferedReader br = new BufferedReader(new FileReader(srcFile))) {
            String line ="";
            while ( (line = br.readLine()) != null){
                if(line.contains("private")) {
                    line.replace("private","private");
                    sb.append(line);
                    sb.append("\n");
                } else {
                    sb.append(line);
                    sb.append("\n");
                }
            }
        }catch (IOException ioe){
            System.err.println(ioe);
        }

        try(BufferedWriter br = new BufferedWriter(new FileWriter(srcFile))) {
            br.append(sb);
        }catch (IOException ioe){
            System.err.println(ioe);
        }
    }
}
