package samples;

import java.io.*;
import java.util.Arrays;

/**
 * Created by andriy on 2/19/17.
 */
public class PushBackDemo {
    public static void main(String[] args) {
        File pushBackFile = new File("files" + File.separator + "pbin.dat");
        byte[] srcBytes1 = new byte[]{'b','r','a','i','n'};
        byte[] srcBytes2 = new byte[]{'h','e','l','l','o'};
        byte[] srcBytes3 = new byte[]{'m','a','r','c','h'};

        try(DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(pushBackFile)))){
            System.out.println("Write to file: ");
            dos.write(srcBytes1);
            System.out.println(new String(srcBytes1));
            dos.write(srcBytes2);
            System.out.println(new String(srcBytes2));
            dos.write(srcBytes3);
            System.out.println(new String(srcBytes3));
        }catch (IOException e){
            System.err.println(e);
        }
        // reading
        System.out.println("Read from file: ");
        PushbackInputStream pushbackInputStream = null;
        try(DataInputStream dis = new DataInputStream(
                pushbackInputStream = new PushbackInputStream(
                        new BufferedInputStream(
                                new FileInputStream(pushBackFile)),10))){ // розмір pushback - 10 byte

                int availabeLenght = pushbackInputStream.available();
                    System.out.println("available bytes: " + availabeLenght);

                byte[] testByte = new byte[5];
                dis.read(testByte); // читаю 1 масив байтів
                System.out.println(new String(testByte));


                // дивлюся в 2 масив байтів
                pushbackInputStream.read(testByte);
                if(testByte[0] == 'h'){
                    //
                    pushbackInputStream.unread(new byte[]{'w','o','r','l','d'});
                } else{
                    pushbackInputStream.unread(testByte); // цей крок зайвий у контексті даного прикладу
                    // це виключно для демонстрації
                }

                dis.read(testByte); // читаю 2 масив байтів
                System.out.println(new String(testByte));
                dis.read(testByte); // читаю 3 масив байтів
                System.out.println(new String(testByte));



            }catch (IOException e){
            System.err.println(e);
            e.printStackTrace();
        }


    }
}
