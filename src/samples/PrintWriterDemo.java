package samples;

import java.io.*;
import java.util.Scanner;

public class PrintWriterDemo {
    public static void main(String[] args) {


        try(PrintWriter pw = new PrintWriter("1.txt","CP1251")){
            pw.write("Привіт світ. Ми любимо Java! \n");
            pw.write("Їжачок у тумані є!\n");
        }catch (IOException e){

        }

       try(BufferedReader isr = new BufferedReader(new InputStreamReader(new FileInputStream("1.txt"),"CP1251"))){
          // try(Scanner isr = new Scanner(new InputStreamReader(new FileInputStream("1.txt"),"CP1251"))){
            // use  Scanner than BufferedReader
                String line;
                while ((line = isr.readLine()) != null){
                    //             while ((line = isr.nextLine() ) != null){
                    System.out.println(line);
                }
        }catch(IOException e){

        }



    }
}
