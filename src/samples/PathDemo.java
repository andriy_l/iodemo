package samples;

import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Created by andriy on 2/19/17.
 */
public class PathDemo {
    public static void main(String[] args) {
       Properties properties = System.getProperties();
       // if you forget names of properties
//       for(String propertyName : properties.stringPropertyNames()){
//           System.out.println(propertyName + " " + properties.getProperty(propertyName));
//       }

        Path relativeSample = Paths.get("usr","share","doc");
        System.out.println("relative " + relativeSample);
        System.out.println(relativeSample.resolve("samba"));

        // Returns a relative Path that is a subsequence of the name elements of this path.
        // The beginIndex and endIndex parameters specify the subsequence of name elements.
        // The name that is closest to the root in the directory hierarchy has index 0.
        // The name that is farthest from the root has index count-1.
        // The returned Path object has the name elements that begin at beginIndex and extend to the element at index endIndex-1.
        //  [startIndex, endIndex)
        System.out.println("Subpath " + relativeSample.subpath(2,3));

        //String homeDir = properties.getProperty("user.home");
        String tmpDir = properties.getProperty("java.io.tmpdir");
        System.out.println("Directory for temporary files is: " + tmpDir);
        Path tmpPath = Paths.get(tmpDir); // OK that homeDir has
        System.out.println("tmpPath " + tmpPath);
        String pathToMySoftDir = "/home/andriy/mysoft";
        Path myDocSoftPath = Paths.get(tmpDir,"soft","documentation");
        System.out.println("path to documentation for my soft: " + myDocSoftPath);
        Path workRelative = Paths.get("work");
        System.out.println("work relative " + workRelative);
        Path workPath = myDocSoftPath.resolveSibling(workRelative);
        System.out.println("workPath after resove " +workPath);

        // створюємо каталог відносно батька родинний (той самий рівень ієрархії)
        Path binPath = workPath.resolveSibling("bin");
        System.out.println("create bin: " + binPath);
            // як перейти в інший каталог - формує шлях
        Path relativizedPath = myDocSoftPath.relativize(binPath);
        System.out.println(relativizedPath);



    }
}
