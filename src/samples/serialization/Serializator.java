package samples.serialization;

import java.io.*;

public class Serializator {
    public boolean serialization(Student s, String fileName) {
        boolean flag = false;

        File f = new File(fileName);

        ObjectOutputStream ostream = null;
        try {
            FileOutputStream fos = new FileOutputStream(f);
            if (fos != null) {
                ostream = new ObjectOutputStream(fos);
                ostream.writeObject(s); // serialization
                flag = true;
            }
        } catch (FileNotFoundException e) {
            System.err.println("The file can not be created: "+ e);
        } catch (NotSerializableException e) {
            System.err.println("Class does not support serialization: " + e);
        } catch (IOException e) {
            System.err.println(e);
        } finally {
            try {
                if (ostream != null) {
                    ostream.close();
                }
            } catch (IOException e) {
                System.err.println("Closing stream error");
            }
        }
        return flag;
    }
    public Student deserialization(String fileName) throws InvalidObjectException {

        File fr = new File(fileName);

        ObjectInputStream istream = null;
        try {
            FileInputStream fis = new FileInputStream(fr);
            istream = new ObjectInputStream(fis);      // deserialization

            Student st = (Student) istream.readObject();
            return st;
        } catch (ClassNotFoundException ce) {
            System.err.println("The class does not exist: " + ce);
        } catch (FileNotFoundException e) {
            System.err.println("File for deserialization does not exist: "+ e);
        } catch (InvalidClassException ioe) {
            System.err.println("The discrepancy between the versions of classes: " + ioe);
        } catch (IOException ioe) {
            System.err.println("I / O error: " + ioe);
        } finally {
            try {
                if (istream != null) {
                    istream.close();
                }
            } catch (IOException e) {
                System.err.println("Closing stream error");
            }
        }
        throw new InvalidObjectException("the object is not restored");
    }
}

