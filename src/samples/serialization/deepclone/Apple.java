package samples.serialization.deepclone;

import java.io.Serializable;

/**
 * Created by andriy on 2/22/17.
 */
public class Apple implements Serializable {
    {
        System.out.println("Apple non-static block init");
    }
    static {
        System.out.println("Apple static block init");
    }
    enum Sort{
        HONEY_CRISP, CHAMPION, AIDARET, GOLDEN, FUJI, LIGOLD, MUTSU;
    }
    private double weight;
    private Sort sort;

    Apple(){
        super();
        System.out.println("default apple constructor");
    }

    Apple(Sort sort, double weight){
    super();
    this.sort = sort;
    this.weight = weight;
        System.out.println("not-default apple constructor");
    }

    public Sort getSort(){
        return sort;
    }

    @Override
    public String toString(){
        return "Apple:"+this.sort+":"+this.weight;
    }
}
