package samples.serialization.deepclone;

import java.io.Serializable;

/**
 * Created by andriy on 2/22/17.
 */
public class Hedgehog implements Serializable{
    {
        System.out.println("Hedgehog non-static block init");
    }
    static {
        System.out.println("Hedgehog static block init");
    }
    private String name;
    private int age;
    private Apple apple;

    Hedgehog(){
        super();
        System.out.println("Default HG constructor");
    }

    Hedgehog(String name, int age){
        this.name = name;
        this.age = age;
        apple = new Apple(Apple.Sort.HONEY_CRISP, 0.5);
        System.out.println("Non Default HG constructor");
    }

    @Override
    public String toString(){
        return "Hedgehog:"+this.name+":"+this.age+":"+this.apple.getSort();
    }
}
