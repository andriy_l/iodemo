package samples.serialization.deepclone;


import java.io.*;

/**
 * Created by andriy on 2/23/17.
 */
public class DeepClone {
    public static void main(String[] args) {
        Hedgehog hedgehog = new Hedgehog("Andriy", 18);
        Hedgehog hedgehogClone = null;

        try{
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(byteArrayOutputStream);
                oos.writeObject(hedgehog);
                oos.close();

            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(byteArrayInputStream);
                hedgehogClone = (Hedgehog) ois.readObject();
                ois.close();
        }catch (IOException|ClassNotFoundException e){
            e.printStackTrace();
        }

        System.out.println("original: " + hedgehogClone + " hashCode: " + hedgehog.hashCode());
        System.out.println("original and clone are equals? " + (hedgehog == hedgehogClone));
        System.out.println("clone: " + hedgehogClone + " hashCode: " + hedgehogClone.hashCode());
    }
}
