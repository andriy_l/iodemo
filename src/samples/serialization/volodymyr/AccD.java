package samples.serialization.volodymyr;

import java.io.IOException;

/**
 * Created by andriy on 2/23/17.
 */
public class AccD {
    public static void main(String[] args) throws IOException{
        AccountingUser accountingUser = new AccountingUser();
        accountingUser.testUsers("Andriy");
        accountingUser.testUsers("Mykolay");
        accountingUser.testUsers("Oxana");
//        accountingUser.testUsers("Оксана");
        accountingUser.printFile();
    }
}
