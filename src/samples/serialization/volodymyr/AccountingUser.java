package samples.serialization.volodymyr;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
public class AccountingUser {
    File file;
    RandomAccessFile fileAccess = null;
    AccountingUser() {
        file = new File("users.txt");
        try {
            fileAccess = new RandomAccessFile(file, "rw");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    void testUsers(String nameUser) {
        try {
            String s = readFile();
            int lastindex = s.length();
            if (s.contains(nameUser)) {         //Якщо такий юзер вже є у файлі то збільшуємо лічильник
                int index = s.lastIndexOf(nameUser) + nameUser.length() + 1;
                Integer count = Integer.parseInt(readCount(index));
                count++;
                fileAccess.seek(index);
                fileAccess.write(count.toString().getBytes());
            } else {        //Якщо юзера немає у файлі
                fileAccess.seek(lastindex);
                fileAccess.write(nameUser.getBytes());
                fileAccess.write(":1".getBytes());
                fileAccess.write("\n".getBytes());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    String readFile() { // Читаємо весь файл
        String res = "";
        try {
            int b = 0;
            fileAccess.seek(0);
            b = fileAccess.read();
            while (b != -1) {
                res = res + (char) b;
                b = fileAccess.read();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }
    String readCount(int index) {  // читаємо лічильник по індексу
        String res = "";
        try {
            int b = 0;
            fileAccess.seek(index);
            b = fileAccess.read();
            while (b != -1 && b != 10) {
                res = res + (char)b;
                b = fileAccess.read();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }
    void printFile(){ // Друкуємо файл
        System.out.println(readFile());
    }
    @Override
    protected void finalize() throws Throwable { // закриваємо потік читання
        fileAccess.close();
    }
}