package samples.serialization;

import java.io.File;
import java.io.InvalidObjectException;
public class RunnerSerialization {
    public static void main(String[] args) {
        // object creation and recording
        Student ob = new Student("MainAcademy", "Andriy", 1, "pa$$w0rd");
        System.out.println(ob);

        String file = "files"+ File.separator+"demo.data";

        Serializator sz = new Serializator();
        boolean b = sz.serialization(ob, file);
        Student.faculty = "GEO"; // changing the value of static-field
        // reading and output object
        Student res = null;
        try {
            res = sz.deserialization(file);
        } catch (InvalidObjectException e) {

            e.printStackTrace();
        }
        System.out.println(res);
    }
}
