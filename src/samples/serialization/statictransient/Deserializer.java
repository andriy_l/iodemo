package samples.serialization.statictransient;

import java.io.*;

/**
 * Created by andriy on 2/24/17.
 */
public class Deserializer {
    public static void main(String[] args) {

        File f = new File("files" + File.separator + "hh.ser");
        Hedgehog h1 = null;
        System.out.println(h1);
        Hedgehog h2 = null;
        System.out.println(h2);
        Hedgehog h3 = null;
        System.out.println(h3);


        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))){
            h1 = (Hedgehog) ois.readObject();
            h2 = (Hedgehog) ois.readObject();
            h3 = (Hedgehog) ois.readObject();
        }catch (IOException|ClassNotFoundException e){
            e.printStackTrace();
        }
        System.out.println(h1);
        System.out.println(h2);
        System.out.println(h3);
    }
}