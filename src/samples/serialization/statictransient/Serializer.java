package samples.serialization.statictransient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by andriy on 2/24/17.
 */
public class Serializer {
    public static void main(String[] args) {

        File f = new File("files" + File.separator + "hh.ser");
        Hedgehog h1 = new Hedgehog("Колючка",5);
        System.out.println(h1);
        Hedgehog h2 = new Hedgehog("Шипучка",8);
        System.out.println(h2);
        Hedgehog h3 = new Hedgehog("Їжачок",2);
        System.out.println(h3);


        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(f))){
            oos.writeObject(h1);
            oos.writeObject(h2);
            oos.writeObject(h3);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
