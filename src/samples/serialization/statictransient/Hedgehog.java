package samples.serialization.statictransient;

import java.io.Serializable;
import java.util.Random;

/**
 * Created by andriy on 2/22/17.
 */
public class Hedgehog implements Serializable{
    private static long hedgehogID = 0L;
    private String name;
    private int age;
    private transient String password;

    public Hedgehog(){
        super();
        System.out.println("Default HG constructor");
    }

    public Hedgehog(String name, int age){
        hedgehogID++;
        this.name = name;
        this.age = age;
        this.password = hedgehogID+"."+new Random().nextDouble();
        System.out.println("Non Default HG constructor");
    }

    @Override
    public String toString(){
        return "Hedgehog:"+hedgehogID+",name:"+this.name+",age:"+this.age+",pw:"+this.password;
    }
}
