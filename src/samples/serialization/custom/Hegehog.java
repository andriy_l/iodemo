package samples.serialization.custom;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Hegehog implements Serializable {
    private String name;
    private int age;

    public Hegehog(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println("hedgehog constructor");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Hegehog{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    private void writeObject(ObjectOutputStream out) throws IOException{
        System.out.println("Custom serialization");
        out.writeInt(this.age);
//        out.writeChars(this.name); // try with this
        out.writeUTF(this.name); // writeUTF(utf-8) != writeChars(utf-16)
    }

    private void readObject(ObjectInputStream in) throws IOException{
        System.out.println("Custom deserialization");
        this.age = in.readInt();
        this.name = in.readUTF();
    }
}
