package samples.serialization.custom;

import java.io.*;

/**
 * Created by andriy on 7/7/17.
 */
public class CustomDemo {
    public static void main(String[] args) {
        byte[] bytes = null;
        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8096);
                ObjectOutputStream objectOutputStream =
                        new ObjectOutputStream(byteArrayOutputStream)){
            Hegehog hegehog = new Hegehog("Yizhachok", 18);
            objectOutputStream.writeObject(hegehog);
            bytes = byteArrayOutputStream.toByteArray();
        }catch (IOException e){
            System.out.println(e);
        }

        try(ObjectInputStream objectInputStream = new ObjectInputStream(
                new ByteArrayInputStream(bytes))){
            Hegehog hegehog = (Hegehog) objectInputStream.readObject();
            System.out.println(hegehog);
        }catch (IOException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
