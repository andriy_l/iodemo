For Jackson 1.x, it contains 6 separate jars for different purpose, in most cases, you just need jackson-mapper-asl

<dependency>
	<groupId>org.codehaus.jackson</groupId>
	<artifactId>jackson-mapper-asl</artifactId>
	<version>1.9.13</version>
</dependency>

// https://mvnrepository.com/artifact/org.codehaus.jackson/jackson-mapper-asl
compile group: 'org.codehaus.jackson', name: 'jackson-mapper-asl', version: '1.9.13'

----------------------------------------------------------------

<!-- https://mvnrepository.com/artifact/org.codehaus.jackson/jackson-core-asl -->
<dependency>
    <groupId>org.codehaus.jackson</groupId>
    <artifactId>jackson-core-asl</artifactId>
    <version>1.9.13</version>
</dependency>

// https://mvnrepository.com/artifact/org.codehaus.jackson/jackson-core-asl
compile group: 'org.codehaus.jackson', name: 'jackson-core-asl', version: '1.9.13'
