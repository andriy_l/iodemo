package samples.serialization.jsonserialization;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;

/**
 * Created by andriy on 7/7/17.
 */
public class DeserializeFromJSON {
    public static void main(String[] args) {

        ObjectMapper mapper = new ObjectMapper();

        try {

            // Convert JSON string from file to Object
            User user = mapper.readValue(new File("user.json"), User.class);
            System.out.println(user);

            // Convert JSON string to Object
            String jsonInString = "{\"age\":18,\"messages\":[\"msg 1\",\"msg 2\"],\"name\":\"Ivan\"}";
            User user1 = mapper.readValue(jsonInString, User.class);
            System.out.println(user1);

        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
