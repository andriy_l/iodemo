package samples.serialization.jsonserialization.views;

/**
 * Created by andriy on 7/7/17.
 */
public class Views {
    public static class NameOnly{};
    public static class AgeAndName extends NameOnly{};
}
