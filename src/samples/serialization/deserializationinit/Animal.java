package samples.serialization.deserializationinit;

import java.io.Serializable;

/**
 * Created by andriy on 2/23/17.
 */
public class Animal extends GhostAnimal{

    static {
        System.out.println("Animal static block");
    }
    {
        System.out.println("Animal non-static block");
    }

    Animal(){
        System.out.println("Animal constructor");
    }
}
