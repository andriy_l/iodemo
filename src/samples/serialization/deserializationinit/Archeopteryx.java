package samples.serialization.deserializationinit;


import java.io.Serializable;

/**
 //singleton serialization sample
 */
public class Archeopteryx implements Serializable{
    private Archeopteryx archeopteryx = null;

    {
        System.out.println("Archeopteryx non-static block");
    }

    static {
        System.out.println("Archeopteryx static block");
    }

    private Archeopteryx(){
        System.out.println("Archeopteryx constructor");
    }

    public static class ArcheopteryxHelper{
        {
            System.out.println("ArcheopteryxHelper non-static block");
        }
        static {
            System.out.println("ArcheopteryxHelper static block");
        }
        ArcheopteryxHelper(){
            System.out.println("ArcheopteryxHelper constructor");
        }
        // thread-safety and try-catch
        private static final Archeopteryx INSTANCE = new Archeopteryx();

    }

    @Override
    public String toString(){
        return "Archeopteryx";
    }

    public static Archeopteryx getInctance(){
        System.out.println("Archeopteryx getInstance");
        return ArcheopteryxHelper.INSTANCE;
    }

    protected Object readResolve(){
        System.out.println("Archeopteryx read-resolve");
        return getInctance();
    }

}
