package samples.serialization.deserializationinit;

import java.io.Serializable;

/**
 * Created by andriy on 2/23/17.
 */
public abstract class GhostAnimal implements Serializable{
    static {
        System.out.println("GhostAnimal static block");
    }
    {
        System.out.println("GhostAnimal non-static block");
    }

    GhostAnimal(){
            System.out.println("GhostAnimal constructor");
    }


}
