package samples.serialization.deserializationinit;

import org.omg.CORBA.Object;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 =============== The Java serialization spec for Java 1.5 said: =============================
 For serializable objects, the no-arg constructor for the first non-serializable supertype is run.
 For serializable classes, the fields are initialized to the default value appropriate for its type.
 Then the fields of each class are restored by calling class-specific readObject methods,
 or if these are not defined, by calling the defaultReadObject method.

 Note that field initializers and constructors are not executed for serializable classes during deserialization.

 ==============================================
 in a few words reason of deserialization without constructor:
 ==============================================
 Deserialization doesn't invoke the constructor because the purpose of it is to express
 the state of the object as it was serialized,
 running constructor code could interfere with that.
 ==============================================

 */
public class DeserInitSample {
    public static void main(String[] args) {

        System.out.println("Serialization");

//        Hedgehog hedgehog1 = new Hedgehog("Andriy", 8);
        GhostAnimal animal = new Animal();
        //singleton serialization sample
//        Archeopteryx archeopteryx = Archeopteryx.getInctance();
        Path path = Paths.get("files","hedgehog.ser");
//        System.out.println("Written Animal: " + archeopteryx);

        if(!(animal instanceof Serializable)){
            System.out.println("We cannot save this object");
            System.exit(0);
        }
        System.out.println("Written Animal: " + animal);
        try(ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(path.toFile()))){
            //oos.writeObject(hedgehog1);
            oos.writeObject(animal);
        }catch (IOException e){
            e.printStackTrace();
        }

        System.out.println("Deserialization");

        Hedgehog hedgehog2 = null;
        Archeopteryx archeopteryx2 = null;
        GhostAnimal animal2 = null;
        try(ObjectInputStream oos = new ObjectInputStream(new FileInputStream(path.toFile()))){
//            hedgehog2 = (Hedgehog) oos.readObject();
//            archeopteryx2 = (Archeopteryx) oos.readObject();
            animal2 = (GhostAnimal) oos.readObject();
        }catch (IOException e){
            e.printStackTrace();
        }catch (ClassNotFoundException c){
            c.printStackTrace();
        }
        System.out.println("Readed HH: " + animal2);
        // How to check if an object implements an interface Serializable
        System.out.println(GhostAnimal.class.isAssignableFrom(Animal.class));




    }
}
