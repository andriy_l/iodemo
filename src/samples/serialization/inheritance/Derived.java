package samples.serialization.inheritance;

import java.io.*;

//class Base implements Serializable{
class Base{
    int code = 99;
    Base(){
        System.out.println("constructor Base");
        code = 77;
    }
}

public class Derived extends Base implements Serializable{
    Derived(){
        System.out.println("constructor Derived");
    }
    public static void main(String[] args) {
        Derived derived = new Derived();
        System.out.println("Class derived created");
        derived.code = 88;
        try{
            ObjectOutputStream oos =
                    new ObjectOutputStream(new FileOutputStream("tests.ser"));
            oos.writeObject(derived);
            oos.close();
            System.out.println("Deserialization");
            ObjectInputStream ois =
                    new ObjectInputStream(new FileInputStream("tests.ser"));
            Derived derived2 = (Derived) ois.readObject();
            System.out.println("deserialized code: "+derived2.code);
            ois.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }
}
