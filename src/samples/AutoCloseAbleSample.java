package samples;

import java.io.*;

/**
 * Created by andriy on 2/18/17.
 */
public class AutoCloseAbleSample {
    public static void main(String[] files) {
    String srcFile = "files"+File.separator+"fw.txt", dstFile = "files"+File.separator+"fw2.txt";
        try (
                BufferedReader inputFile = new BufferedReader(new FileReader(srcFile));

             BufferedWriter outputFile = new BufferedWriter(new FileWriter(dstFile));
        ) { //TRY

            int ch = 0;

            while ( (ch = inputFile.read()) != -1) {

                outputFile.write( ch );

            }

        } catch (IOException exception) {
            System.err.println("Error in opening or processing file " + exception.getMessage());

        }
    }
}
