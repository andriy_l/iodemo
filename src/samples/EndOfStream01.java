package samples;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class EndOfStream01 {

    public static void main(String[] args) {

        int a = 2;
        int b = 4;
        int c = 8;
        FileOutputStream outputStream;
        try {
            outputStream = new FileOutputStream("files" + File.separator + "file.txt");
            outputStream.write(a);
            outputStream.write(b);
            outputStream.write(c);
            outputStream.flush();
            outputStream.close();
        }catch (IOException e) {
            System.out.println(e);
        }

        try {
            try(FileInputStream input = new FileInputStream("files"+ File.separator+"file.txt")) {
                int data = input.read();
                while(data != -1){
                    System.out.println( data);
                    data = input.read();
                }
                System.out.println("if data ended we get: " + input.read());
                System.out.println("and after end we get: " + input.read());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

//        try(FileReader fileReader = new FileReader("file.smth")) {
//            while ((ch = inputFile.read()) != VAL) {
//
//                outputFile.write((char) ch);
//
//            }
//        }
    }
}
