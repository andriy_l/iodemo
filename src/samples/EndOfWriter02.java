package samples;

import java.io.*;
import java.util.Arrays;

public class EndOfWriter02 {

    public static void main(String[] args) {
        // число з народного фольклору
        int d = 1000500;


        byte[] src = new byte[20];
        Arrays.fill(src,(byte)5);
        Arrays.fill(src, 5,15,(byte) 10);
        byte[] dst = new byte[100];

        BufferedOutputStream outputStream;
        try {
            outputStream = new BufferedOutputStream(new FileOutputStream("files" + File.separator + "fileFOS.txt"));
            outputStream.write(d);
            outputStream.write(src);
            outputStream.flush();
            outputStream.close();
        }catch (IOException e) {
            System.out.println(e);
        }

        try {
            try(
                    BufferedInputStream input = new BufferedInputStream(new FileInputStream("files"+ File.separator+"fileFOS.txt"));
                    BufferedInputStream input2 = new BufferedInputStream(new FileInputStream("files"+ File.separator+"fileFOS.txt"));
                    BufferedInputStream input3 = new BufferedInputStream(new FileInputStream("files"+ File.separator+"fileFOS.txt"))
            ) {
                // приклади зчитування потоків
                // 1) read in this way
                int data = input.read();
                while(data != -1){
                    System.out.println( data);
                    data = input.read();
                }
                // 2) or read in this way
                int bytesAvailable = input2.available();
                if(bytesAvailable > 0){
                    byte[] data2 = new byte[bytesAvailable];
                    input2.read(data2);

                    System.out.println(Arrays.toString(data2));
                }
                // 3) READ TWICE SAME DATA with mark and reset
                System.out.println("Stream Marking support? " + input3.markSupported());
                input3.mark(Integer.MAX_VALUE);
                    System.out.println("Read at first");
                    if(bytesAvailable > 0){
                        byte[] data3 = new byte[bytesAvailable];
                        input2.read(data3);
                        System.out.println(Arrays.toString(data3));
                    }

                try{
                        input3.reset();
                    System.out.println("Read at twice after reset");
                    if(bytesAvailable > 0){
                        byte[] data4 = new byte[bytesAvailable];
                        input2.read(data4);
                        System.out.println(Arrays.toString(data4));
                    }
                }catch (IOException ioe){
                    ioe.printStackTrace();
                }

                System.out.println("if data ended we get: " + input.read());
                System.out.println("and after end we get: " + input.read());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
