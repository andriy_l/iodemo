package samples;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by andriy on 2/19/17.
 */
public class ZipDemo {
    public static void main(String[] args) {

        File myZipFile = new File("files" + File.separator + "my.zip");
        // write
        try(ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(myZipFile))){

        }catch (IOException e){
            e.printStackTrace();
        }

        // read

        try(ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(myZipFile))){

            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null){
                System.out.println("entry " + entry.getName() + " " + entry.getSize());
                Object o = entry.clone();
            }

        }catch (IOException ioe){
            ioe.printStackTrace();
        }

    }
}
