package samples.channels;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by andriy on 7/2/17.
 */
public class RAFChannelDemo {
    public static void main(String[] args) {
        try(RandomAccessFile aFile = new RandomAccessFile("files/nio-data.txt", "rw");
        FileChannel inChannel = aFile.getChannel()) {

            ByteBuffer buf = ByteBuffer.allocate(48);

            // Reads a sequence of bytes from this channel into the given buffer.
            int bytesRead = inChannel.read(buf);
            while (bytesRead != -1) {

                System.out.println("Read " + bytesRead);

                // перевернути
                // Flips this buffer.
                // The limit is set to the current position and then the position is set to zero.
                buf.flip();

                while (buf.hasRemaining()) {
                    System.out.print((char) buf.get());
                }

                buf.clear();
                bytesRead = inChannel.read(buf);
            }
         //   aFile.close();
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
}
