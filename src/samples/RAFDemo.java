package samples;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;

/**
 * Created by andriy on 2/19/17.
 */
public class RAFDemo {
    public static void main(String[] args) {

        byte[] dog = new byte[]{'d','o','g'};
        byte[] cat = new byte[]{'c','a','t'};
        byte[] hhog = new String("hedgehog").getBytes();
        byte[] hhogUA = new String("їжачок").getBytes();
        byte[] inThe = new String("in the").getBytes();
        byte[] fog = new String("fog").getBytes();
        File f = new File("files" + File.separator+"raf.dat");
        if(f.exists()) {
            f.delete();
        }

        try(
                RandomAccessFile out = new RandomAccessFile("files" + File.separator+"raf.dat","rw");
                RandomAccessFile in = new RandomAccessFile("files" + File.separator+"raf.dat","rw");
        ){
            // write to the end ;-)
             // out.seek(out.length());

            out.write(dog);
            out.write(cat);
            out.write(hhog);
            out.write(' ');
            out.write(inThe);
            out.write(' ');
            out.write(fog);
            String str = "Hello ";
            out.writeChars(str); // 5 chars x 2 : every char takes 2 positions
            out.write(hhogUA);
            System.out.println("final file size is: " + out.length());

            System.out.println("At beginning file pointer is on " + in.getFilePointer());

            long position = dog.length + cat.length;
            in.seek(position);
            System.out.println("seek to " + position + " "+ in.getFilePointer());
            int totalLenght = (int)position + hhog.length + inThe.length + fog.length + 2 + (str.length()*2) + hhogUA.length;
            byte[] bytes = new byte[totalLenght];
            System.out.println("записано символів " + totalLenght);
            in.read(bytes);
            System.out.println("as bytes arr: " + Arrays.toString(bytes));
            System.out.println("as characters arr: " + Arrays.toString(new String(bytes).toCharArray()));
            System.out.println("as string: " + new String(bytes));
            System.out.println("А що ж там за \"горизонтом\"? після " + in.getFilePointer());
            System.out.println("А що ж там за \"горизонтом\"? 1 " + in.read());
            System.out.println("А що ж там за \"горизонтом\"? 2 " + in.readLine());



        }catch (IOException e){

        }
    }
}
