package samples;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by andriy on 2/18/17.
 */
public class Console {
    public static void main(String[] files) {
        java.io.Console cons;
        String username;
        char[] passwd;
        if((cons = System.console()) == null){
            System.out.println("Console is null run in real system");
            System.exit(0);
        }

        if (       (cons = System.console()) != null
                && (username = cons.readLine("[%s]", "Username:")) != null
                && (passwd = cons.readPassword("[%s]", "Password:")) != null){
            System.out.println(username);
            System.out.println(passwd);
        }
    }

}
