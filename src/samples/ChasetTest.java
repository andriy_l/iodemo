package samples;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by andriy on 2/19/17.
 */
public class ChasetTest {
    public static void main(String[] args) {
        // list available encodings
        Map<String, Charset> charsetMap = Charset.availableCharsets();
        for(String name : charsetMap.keySet()){
            System.out.println(name);
        }

        Charset charsetCP1251 = Charset.forName("CP1251");
        // convert from characters to bytes
        String str = "abcd";
        ByteBuffer byteBuffer = charsetCP1251.encode(str);
        byte[] bytes = byteBuffer.array();
        System.out.println(Arrays.toString(bytes));

        //convvert from bytes to characters
        ByteBuffer byteBuffer2 = ByteBuffer.wrap(bytes,0,bytes.length);
        CharBuffer charBuffer = charsetCP1251.decode(byteBuffer2);
        String str2 = charBuffer.toString();
        System.out.println(str2);
    }
}
