package samples.zip;

import java.io.File;
import java.io.FileNotFoundException;
public class PackDemo {
    public static void main(String[] args) {
        String dirName = "files";
        PackJar pj = new PackJar("example.jar");
        try {
            pj.pack(dirName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

