package samples.zip;
public class UnpackDemo {
    public static void main(String[] args) {
        // розташування архіву

        String nameJar = "example.jar";
        // куди файли будуть розпаковані

        String destinationPath = System.getProperty("java.io.tmpdir");
        new UnPackJar().unpack(destinationPath, nameJar);

    }
}

