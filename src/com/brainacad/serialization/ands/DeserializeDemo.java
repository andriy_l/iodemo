package com.brainacad.serialization.ands;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializeDemo {
    public static void main(String[] args) {
        File file = new File("Files" + File.separator + "employee.ser");
        Employee Igor = new Employee("Igor","Ternopil",12,123456789);
        try (FileInputStream fos = new FileInputStream(file);
             ObjectInputStream oos = new ObjectInputStream(fos)) {
            System.out.println(oos.readObject());
//            System.out.println(Igor.getPassport());
            System.out.println(Igor.getApple());
        } catch (IOException | ClassNotFoundException ioe) {
            ioe.printStackTrace();
        }
    }
}
