package com.brainacad.serialization.ands;

public class Apple{
    private double weight;
    public double getWeight() {
        return weight;
    }
    public Apple(double weight) {
        this.weight = weight;
    }
    @Override
    public String toString() {
        return "Apple{" +
                "weight=" + weight +
                '}';
    }
}