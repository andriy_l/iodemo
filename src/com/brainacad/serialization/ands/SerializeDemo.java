package com.brainacad.serialization.ands;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializeDemo {
    public static void main(String[] args) {
        File file =  new File("Files" +File.separator +"employee.ser");
        Employee Igor = new Employee("Igor","Ternopil",12,123456789);
        try(FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeObject(Igor);
//            Igor.setPassport("wqe");
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }
}
