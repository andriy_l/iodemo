package com.brainacad.serialization.ands;

import java.io.Serializable;

public class Employee implements Serializable {
    private static final long serialVersionUID = -3829395164481815361L;
    private String name;
    private String address;
    private int SSN;
    private int number;
    private String passport;
    private transient Apple apple;

    public Apple getApple() {
        return apple;
    }

    public Employee() {
    }

    public Employee(String name, String address, int SSN, int number) {
        this.name = name;
        this.address = address;
        this.SSN = SSN;
        this.number = number;
        this.passport = passport;
        this.apple = new Apple(.700);
    }
}
