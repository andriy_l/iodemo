package com.brainacad.serialization.serializeuser;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andriy on 2/23/17.
 */
public class Main {
    public static void main(String[] args) {
        Path filePath = Paths.get("files","user.ser");
        User andriy = new User("L","V",18);
        User serhiy = new User("Andriy","Lutskiv",10);
        User hedgehog = new User("Андрій","Луцків",10);
        saveUser(andriy, filePath);
        saveUser(serhiy, filePath);
        saveUser(hedgehog, filePath);
        List<User> users = readUsers(filePath);
        for(User user : users){
            System.out.println(user);
        }
    }

    public static List<User> readUsers(Path filePath){
        List<User> users = new ArrayList<>();
        try(RandomAccessFile randomAccessFile = new RandomAccessFile(filePath.toFile(),"r")){
            randomAccessFile.seek(0);
            String readedLine;
            while ((readedLine = randomAccessFile.readLine())!= null) {
                System.out.println(readedLine);
                String[] tmpLine = readedLine.split(",");
                users.add(new User(tmpLine[0], tmpLine[1], Integer.parseInt(tmpLine[2])));
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return users;
    }

    public static void saveUser(User user, Path filePath){
        try(RandomAccessFile randomAccessFile = new RandomAccessFile(filePath.toFile(),"rw")){
            randomAccessFile.seek(randomAccessFile.length());
            randomAccessFile.writeBytes(user.toString());
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
