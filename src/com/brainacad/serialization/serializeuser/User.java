package com.brainacad.serialization.serializeuser;

import java.io.Serializable;

/**
 * Created by andriy on 2/23/17.
 */
public class User implements Serializable{
    private static final long serialVersionUID = 2831463958643960429L;
    private String firstName;
    private String lastName;
    private int age;

    User(){}

    public User(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString(){
        return this.firstName+","+this.lastName+","+age+"\r\n";
    }
}
