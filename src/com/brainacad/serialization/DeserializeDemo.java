package com.brainacad.serialization;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;

public class DeserializeDemo {
    ArrayList al;
    public static void main(String[] args) {
        File f = new File("files" + File.separator + "employee.ser");
        Employee charlesClone = null;
        Employee andriyClone = null;
        try(FileInputStream fos = new FileInputStream(f);
            ObjectInputStream oos = new ObjectInputStream(fos)){
            andriyClone = (Employee) oos.readObject();
            System.out.println(andriyClone + "pass " + andriyClone.getPassport());
            Date d = andriyClone.getBirthDate();

            System.out.println("SSN " + andriyClone.getSSN() + " Date " + d);
            System.out.println("I have " + (andriyClone.getMyApple()).getWeight());
//AC ED 00 05

// same as previously saved object with serial number X
        }catch (IOException e){
            e.printStackTrace();
        }catch (ClassNotFoundException cnef){
            cnef.printStackTrace();
        }

    }
}
