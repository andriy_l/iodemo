package com.brainacad.serialization.externalizeuser;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by andriy on 2/23/17.
 */
public class M {
    public static void main(String[] args) {
        Path filePath = Paths.get("files","user.ser");
        User[] users = new User[]{new User("L","V",18), new User("Andriy","Lutskiv",10), new User("Андрій","Луцків",10)};
        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(filePath.toFile()))){
            for(User u : users)
                objectOutputStream.writeObject(u);
        }catch (IOException e){
            e.printStackTrace();
        }


        ArrayList<User> arrayList = new ArrayList<>();
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath.toFile()))){
            User u;try {
            while ((u = (User) ois.readObject()) != null) {

                    arrayList.add(u);

            }
            }catch (EOFException e){
                System.out.println("EOF");
        }
        }catch(IOException|ClassNotFoundException e){
            e.printStackTrace();
        }
        User[] users2 = new User[arrayList.size()];
        int i = 0;
        for (User u : arrayList){
            users2[i++]= u;
            System.out.println(u);
        }
        System.out.println("users readed: " + users2.length);

    }
}
