package com.brainacad.serialization;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by andriy on 22.02.17.
 */
public class Employee implements Serializable {

private static final long serialVersionUID = -4548707261698852291L;
    private String name;
    private transient Apple myApple;
    private String address;
    private transient int SSN;
    private transient Date birthDate;
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }


    private int number;
    private String passport;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSSN() {
        return SSN;
    }

    public void setSSN(int SSN) {
        this.SSN = SSN;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public Apple getMyApple() {
        return myApple;
    }

    public Employee(){


    }

    public Employee(String name, String address, int SSN, int number) {
        this.name = name;
        this.address = address;
        this.SSN = SSN;
        this.number = number;
        this.passport = passport;
        this.myApple = new Apple(.800); // 0.800
    }

    public String toString(){
        return this.name + " " + this.address + " " +this.SSN + " " + this.number;
    }

}
