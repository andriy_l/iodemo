package com.brainacad.serialization;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;

/**
 * Created by andriy on 22.02.17.
 */
public class SerializeDemo {
    public static void main(String[] args) {
        File f = new File("files" + File.separator + "employee.ser");
        Employee charles2 = new Employee("Charles", "Africa, Nigeria", 10_000, 55_5_5);
//        Employee andriy2 = new Employee("Andriy", "Ternopil", 11_000, 66_5_5);
//        charles2.setPassport("MC908098");
//        andriy2.setPassport("NU908098");
        Date d = new Date();
        System.out.println("current time:" + d);
        charles2.setBirthDate(d);
        try(FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream(fos)){
            oos.writeObject(charles2);
//            oos.writeObject(andriy2);
//AC ED 00 05
        }catch (IOException e){
            e.printStackTrace();
        }

    }
}
