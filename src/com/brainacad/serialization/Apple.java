package com.brainacad.serialization;

import java.io.Serializable;

/**
 * Created by andriy on 22.02.17.
 */
public class Apple  {
    private double weight;

    public double getWeight() {
        return weight;
    }

    public Apple(double weight) {
        this.weight = weight;
    }
}
