package com.brainacad.io;

import java.io.*;

/**
 * Created by andriy on 20.02.17.
 */
class Hedgehog implements Serializable{
    private String name;
    Hedgehog(){}
    public void setName(String name){
        this.name = name;
    }
    public String toString(){
        return "Ya yizhachok!"+this.name;
    }
}
public class M {
    public static void main(String[] args)  {
        File f = new File("files" + File.separator + "v.dat");
        String str1 = "Serhiy";
        Hedgehog h1 = new Hedgehog();
        h1.setName("Andriy");
        ObjectOutputStream dos = null;
        try {
            dos = new ObjectOutputStream(
                    new FileOutputStream(f));
        }catch (IOException ioe){}
        try {
        dos.writeObject(str1);
        dos.writeObject(h1);

            dos.close();
        }catch (IOException e){}
        System.out.println(f.length());
        String str2;
        Hedgehog h2 = null;
        try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f))){
            str1 = (String) ois.readObject();
            h2 = (Hedgehog) ois.readObject();
        }catch (IOException | ClassNotFoundException ioe){
            ioe.printStackTrace();
        }
        System.out.println(str1 + h2);
    }
}
