package com.brainacad.io.rafdemo;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Created by andriy on 20.02.17.
 */
public class AccountingUser {
    private RandomAccessFile randomAccessFile = null;
    private String fileName = "users.txt";

    AccountingUser(){
        try{
            randomAccessFile =
                    new RandomAccessFile("files" + File.separator + fileName, "rw");
        }catch(
                IOException e){
            System.err.println(e);
        }

    }


    public void testUsers(String nameUser) {
        try {
            boolean oldUser = false;
            // if file is empty and no users were added
            if (randomAccessFile.length() == 0) {
                System.out.println("file is empty");
                randomAccessFile.writeChars(nameUser + ":");
                randomAccessFile.writeInt(1);
                return;
            }

            randomAccessFile.seek(0);

            while (randomAccessFile.length() > randomAccessFile.getFilePointer()) {
                String existedUser = getUserName(randomAccessFile.getFilePointer());
                long beforeIntPos = randomAccessFile.getFilePointer();
                int count = randomAccessFile.readInt();

                if (existedUser.equals(nameUser)) {
                    oldUser = true;
                    randomAccessFile.seek(beforeIntPos);
                    randomAccessFile.writeInt(++count);
                    break;
                }
            }
                if(!oldUser){
                randomAccessFile.writeChars(nameUser + ":");
                randomAccessFile.writeInt(1);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // helper function for
    private String getUserName(long startPos) throws IOException{
        String str = "";
        char ch;
        randomAccessFile.seek(startPos);
        while ((ch = randomAccessFile.readChar()) != ':') {
            str += ch;
        }
        return str;
    }


    public void printFile(){
        try {
            if(randomAccessFile.length() == 0) {
                System.out.println("File is empty");
                System.exit(0);
            }
            randomAccessFile.seek(0);
            while (randomAccessFile.length() > randomAccessFile.getFilePointer()) {
                System.out.println(getUserName(randomAccessFile.getFilePointer()) + ":" + randomAccessFile.readInt());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void finalize(){
        try {
            randomAccessFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
