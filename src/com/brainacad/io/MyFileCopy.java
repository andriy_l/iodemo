package com.brainacad.io;

import java.io.*;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by andriy on 20.02.17.
 */
public class MyFileCopy {
    public static final int SIZE = 1024 * 8;
    public static void main(String[] args) throws IOException {

        // File src = new File(args[0]);
        File src = new File("/home/data/books/Architectura_computer_Andrew_Tanenbaum.pdf");
        // File dst = new File(args[1]);
        File dst = new File("/tmp/result.pdf");

        Path srcPath = src.toPath();
        Path dstPath = dst.toPath();
        Files.copy(srcPath,dstPath);
//        byte[] bytes = Files.readAllBytes(srcPath);
//        String fileAsString = Files.readAllLines(srcPath, );

// simplest most non-effective method
//        try(BufferedInputStream fileInputStream = new BufferedInputStream(new FileInputStream(src));
//        BufferedOutputStream fileOutputStream = new BufferedOutputStream(new FileOutputStream(dst));){
//            int readed;
//            while((readed = fileInputStream.read()) != -1) {
//                fileOutputStream.write(readed);
//            }
//        }catch (IOException ioe){
//            System.err.println(ioe);
//        }
        // the most effective method
//        try(FileInputStream fSRC = new FileInputStream( src );
//        FileOutputStream fDST = new FileOutputStream( dst );) {
//            FileChannel chSRC = fSRC.getChannel();
//            FileChannel chDST = fDST.getChannel();
//            MappedByteBuffer mb = chSRC.map(FileChannel.MapMode.READ_ONLY, 0L, chSRC.size());
//            byte[] barray = new byte[SIZE];
//            mb.order(ByteOrder.BIG_ENDIAN);
//            long checkSum = 0L;
//            int nGet;
//            while (mb.hasRemaining()) {
//                nGet = Math.min(mb.remaining(), SIZE);
//                mb.get(barray, 0, nGet);
//                chDST.write(mb);
//            }
//        }catch (IOException ioe){
//            System.err.println(ioe);
//        }

    }
}
