package com.brainacad.io;

import java.io.File;
import java.util.Properties;

/**
 * Created by andriy on 20.02.17.
 */
public class MyFilesList {
    public static void main(String[] args) {
        if (args.length == 0) {

            String homeDirStr = System.getProperty("user.home");
            String tmpDirStr = System.getProperty("java.io.tmpdir");
            File homeDir = new File(homeDirStr); // C:\\
            listFiles(homeDir);
        } else {
            listFiles(new File(args[0]));
        }
    }
    public static void listFiles(File f){
        File[] files = f.listFiles();
        for (File file : files){
            if(file.isDirectory()){
                listFiles(file);
            }
        System.out.println(f.getName());
        }
    }
}
