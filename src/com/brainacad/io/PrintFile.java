package com.brainacad.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by andriy on 20.02.17.
 */
public class PrintFile {
    public static void main(String[] args) {
                  File f = new File("/etc/passwd");
        System.out.println("Buffered");
        try(BufferedReader br = new BufferedReader(new FileReader(f))){
            String line = "";
            while ((line = br.readLine()) != null ){
                System.out.println(line);
            }
        }catch (IOException ioe) {
            System.err.println(ioe);
        }
        System.out.println("Scanner");
        try(Scanner scanner = new Scanner(f)){ // System.in
            while (scanner.hasNext()) {
                System.out.println(scanner.nextLine());
            }
        }catch (IOException ioe){
            System.err.println(ioe);
        }

    }
}
