package demo;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

public class WebStatDB {

    public static void main(String[] args) {
        File f_db;
        int users_count = 0, visit_count;
        HashMap<String, UserStats> users = new HashMap<String, UserStats>();
        Scanner scn;
        String user_name;
        UserStats user_stat;

        f_db = new File(WebStatDB.class.getName() + ".db");
        try (RandomAccessFile db = new RandomAccessFile(f_db, "rw")) {
            System.out.println("Loading DB...");
            if (f_db.length() > 0) {
                users_count = db.readInt();
            }
            System.out.println("Users count: " + users_count);
            // TODO LoadDB in separate class UsersDB
            for (int i = 0;i < users_count;i++) {
                user_name = db.readUTF();
                visit_count = db.readInt();
                user_stat = new UserStats(user_name, visit_count);
                users.put(user_stat.UserName, user_stat);
                user_stat.print();
            }
            scn = new Scanner(System.in);
            do {
                System.out.print("Enter user name: ");
                user_name = scn.nextLine();
                if (user_name.length() == 0) break;
                if (users.containsKey(user_name)) {
                    user_stat = users.get(user_name);
                }
                else {
                    user_stat = new UserStats(user_name);
                    users.put(user_stat.UserName, user_stat);
                }
                user_stat.visit();
                System.out.println("User: " + user_stat.UserName + " visits count: " + user_stat.VisitsCount);
            } while (user_name.length() != 0);
            // TODO SaveDB in separate class UsersDB
            System.out.println("Saving DB...");
            db.seek(0);
            db.writeInt(users.size());
            for (UserStats user_stat2 : users.values()) {
                //db.writeInt(user_stat2.UserName.length());
                db.writeUTF(user_stat2.UserName);
                db.writeInt(user_stat2.VisitsCount);
            }
            System.out.println("Stored " + users.size() + " users stats.");
        } catch (IOException e) {
            System.out.println("Exception: " + e.toString());
            e.printStackTrace();
        }
    }
}

class UserStats {
    public final String UserName;
    public int VisitsCount;


    public UserStats(String user_name) {
        UserName = user_name;
        VisitsCount = 0;
    }

    public UserStats(String user_name, int visits_count) {
        UserName = user_name;
        VisitsCount = visits_count;
    }

    public void visit() {
        VisitsCount++;
    }
    public void print() {
        System.out.println(UserName + " - " + VisitsCount);
    }
}
