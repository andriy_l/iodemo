package demo;
import java.io.*;

public class Streams {

    public static void main(String[] args)  {

        try  {

            FileOutputStream fos = new FileOutputStream("serial");

            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeInt(5);

            oos.flush();

            oos.close();

        }  catch(Exception e)  {

            System.out.println("Serialization:");

            System.exit(0);

        }

        try {

            int  z;

            FileInputStream fis = new FileInputStream("serial");

            ObjectInputStream ois = new ObjectInputStream(fis);

            z = ois.readInt();

            ois.close();

            System.out.println(z);

        }  catch (Exception e) {

            System.out.print("Deserialization");

            System.exit(0);

        }

    }

}